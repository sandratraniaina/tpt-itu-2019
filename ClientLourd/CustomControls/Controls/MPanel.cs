﻿using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using CustomControls.Utils;
using System.ComponentModel;

namespace CustomControls.Controls
{
    public partial class MPanel : Panel
    {

        private Color background = Color.FromArgb(255, 0, 29, 45);
        private int cornerRadius = 5;
        private int _scrollWidth = 35;

        [Category("Custom")] public Color Background { get => background; set { BackColor = Color.Transparent; background = value; Refresh(); } }
        [Category("Custom")] public int CornerRadius { get { return cornerRadius; } set { cornerRadius = value; Refresh(); } }
        [Category("Custom")] public int ScrollWidth { get => _scrollWidth; set { _scrollWidth = value; Refresh(); } }
        [Category("Custom")] public new bool AutoScroll { get => container.AutoScroll; set { container.AutoScroll = value; } }

        public MPanel()
        {
            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Bitmap B = new Bitmap(Width, Height);
            Graphics G = Graphics.FromImage(B);
            Rectangle x2 = new Rectangle(0, 0, Width, Height);
            ColorBlend myblend = new ColorBlend();
            myblend.Colors = new[] { background, background }; // colors
            myblend.Positions = new[] { 0.0F, 1.0F };
            LinearGradientBrush lgBrush = new LinearGradientBrush(x2, Color.Black, Color.Black, 90.0F);
            lgBrush.InterpolationColors = myblend;
            G.SmoothingMode = SmoothingMode.HighQuality;
            Tool.FillRoundedRectangle(G, lgBrush, x2, cornerRadius);
            e.Graphics.DrawImage(B.Clone() as Image, 0, 0);
            G.Dispose(); B.Dispose();
        }

        private void OnSizeCHanged(object sender, System.EventArgs e)
        {
            if (AutoScroll)
            {
                container.Width = Width + _scrollWidth;
            }
            else
            {
                container.Width = Width;
            }
        }
    }
}
