﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Windows.Forms;
using WinFormTestTPT.Controller.Custom;
using WinFormTestTPT.Models;
using WinFormTestTPT.Util;

namespace WinFormTestTPT.Views
{
    public partial class LoginFormV2 : CustomForm
    {
        public static LoginFormV2 Instance { get; set; }

        public LoginFormV2()
        {
            InitializeComponent();
            Init();
            Instance = this;
        }

        void Init()
        {
            label_info.Text = "";
            var remember_me = Properties.Settings.Default.remember_me;
            checkBoxRememberMe.Checked = remember_me;
            if (remember_me)
            {
                inputEmail.Text = Properties.Settings.Default.user_login;
            }
        }

        private void button_login_Click(object sender, EventArgs e)
        {
            label_info.Text = "Tentative de connexion...";
            button_login.Enabled = false;
            button_cancel.Enabled = false;
            NameValueCollection param = new NameValueCollection();
            param.Add("name", inputEmail.Text);
            param.Add("password", inputPwd.Text);
            new WSManager<User>("/user/auth/signin", WebMethode.POST, param).Get((user) =>
            {
                Session.User = user;
                if (Session.User)
                {
                    label_info.Text = "";
                    inputPwd.Text = "";
                    button_login.Enabled = true;
                    button_cancel.Enabled = true;
                    if (checkBoxRememberMe.Checked)
                    {
                        Properties.Settings.Default.user_login = inputEmail.Text;
                        Properties.Settings.Default.Save();
                    }
                    MainForm main = new MainForm();
                    main.Show(this);
                    this.Hide();
                }
                else
                {
                    inputPwd.Text = "";
                    label_info.Text = "Impossible de se connecter, Vérifiez les champs avant de continuer...";
                    button_login.Enabled = true;
                    button_cancel.Enabled = true;
                }
            });
        }

        private void checkBoxRememberMe_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            Properties.Settings.Default.remember_me = cb.Checked;
            Properties.Settings.Default.Save();
        }
    }
}
