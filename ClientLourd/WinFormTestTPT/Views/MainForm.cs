﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinFormTestTPT.Controller.Custom;
using WinFormTestTPT.Models;
using WinFormTestTPT.Util;

namespace WinFormTestTPT.Views
{
    public partial class MainForm : CustomForm
    {
        public static MainForm Instance { get; private set; }
        public ContextMenu contextMenu1 = new ContextMenu();
        private MenuItem mi_exit = new MenuItem("Q&uitter");
        private MenuItem mi_logout = new MenuItem("S&e déconnecter");
        private MenuItem mi_setting = new MenuItem("P&aramètres");
        private List<Platform> platforms = new List<Platform>();
        private List<Game> games = new List<Game>();
        private MetaUser MetaUser = null;

        private Game SelectedGame = null;
        private int IndexGame = 0;

        public MainForm()
        {
            Instance = this;
            InitializeComponent();
            createIconMenuStructure();
            Init();
        }

        public override void OnExit()
        {
            if (Owner != null)
                Owner.Show();
            notifyIcon.Dispose();
        }

        public void createIconMenuStructure()
        {
            // Add menu items to shortcut menu.
            contextMenu1.MenuItems.Add(mi_logout);
            contextMenu1.MenuItems.Add(mi_setting);
            contextMenu1.MenuItems.Add("-");
            contextMenu1.MenuItems.Add(mi_exit);

            // edit menu items events
            mi_exit.Click += Mi_exit_Click;
            mi_logout.Click += Mi_logout_Click;

            // Set properties of NotifyIcon component.
            notifyIcon.ContextMenu = contextMenu1;
        }

        internal async void ProcessDameDownlad(Game game)
        {
            //check if game exist in folder
            if (!DownloadManager.Instance.GameExistInFolder(game))
            {
                await DownloadManager.Instance.DownloadFile(game, "rattack_setup", ".exe", game.title.Trim('\r', '\n'), labelDownloadProgress, () =>
                {
                    RefreshGameCollectionList();
                });
            }
            else
            {
                MessageBox.Show("Le jeu est déjà dans votre bibliothèque.");
            }
        }

        private async void Init()
        {
            labelHeaderUserName.Text = Session.User.username;
            label_fullName.Text = Session.User.name + " " + Session.User.lastname;
            label_mail.Text = Session.User.email;
            label_member_starting_from.Text = Session.User.datesignup.Year + "";
            label_user_desc.Text = Session.User.description;

            //load platforms
            GameCollection gc = await new WSManager<GameCollection>("/game/allGame", WebMethode.GET).GetAsync();
            games = gc.game.ToList();
            Console.WriteLine(games.Count);
            
            platforms.Clear();
            platforms.Add(new Platform() { label = "", id = -1 });
            platforms.AddRange(games.SelectMany(g => g.Platforms).Distinct());
            combo_find_plat.DataSource = platforms.Select(p => p.label).Distinct().ToList();

            //loadLibrary
            await FilterGames(null, null);
            SelectedGame = games[IndexGame];
            LoadSelectedGame();

            //load random games
            Random r = new Random();
            Game[] randomGames = games.OrderBy(x => r.Next()).Take(3).ToArray();
            uiFavoriteGame1.Game = randomGames[0];
            uiFavoriteGame2.Game = randomGames[1];
            uiFavoriteGame3.Game = randomGames[2];

            //create game folder
            if (!Directory.Exists(Constants.GAME_DIRECTORY_NAME))
            {
                Directory.CreateDirectory(Constants.GAME_DIRECTORY_NAME);
            }

            //load existing games in that folder
            DownloadManager.Instance.RefreshFilesIntegrity();
            MetaUser = DownloadManager.Instance.GetMetaUser();
            RefreshGameCollectionList();
        }

        public void RefreshGameCollectionList()
        {
            List<Game> gameCollection = games.Where(g => MetaUser.gameCollection.Any(l => l.Equals(g.id))).ToList();
            fl_gameCollection.Controls.Clear();
            foreach (Game g in gameCollection)
            {
                UIBiblioGame uibg = new UIBiblioGame();
                uibg.Init(g);
                uibg.OnDetails += OnDetailBiblioGame_Click;
                fl_gameCollection.Controls.Add(uibg);
            }
        }

        private void Mi_logout_Click(object sender, EventArgs e)
        {
            Owner.Show();
            Owner.Activate();
            Dispose();
        }

        private void Mi_exit_Click(object sender, EventArgs e)
        {
            Owner.Dispose();
            Dispose();
        }

        private void buttonDetailRecentGame_Click(object sender, EventArgs e)
        {
            FicheForm fiche = new FicheForm();
            fiche.Init(SelectedGame);
            fiche.ShowDialog(this);
        }

        public override void ExitLabel_Click(object sender, EventArgs e)
        {
            Hide();
        }
        private void OnResumeNotif(object sender, EventArgs e)
        {
            Show();
        }

        private void OnDetailFavoriteGame_Click(object sender, EventArgs e)
        {
            UIFavoriteGame fav = sender as UIFavoriteGame;
            if (fav == null) return;
            FicheForm fiche = new FicheForm();
            fiche.Init(fav);
            fiche.ShowDialog(this);
        }

        //do not remove
        private void OnDetailBiblioGame_Click(object sender, EventArgs e)
        {
            Game g = (UIBiblioGame)sender;
            FicheForm fiche = new FicheForm();
            fiche.Init(g);
            fiche.ShowDialog(this);
        }

        private async void buttonFind_Click(object sender, EventArgs e)
        {
            string r_title = txt_find_title.Text.ToLower();
            string r_platform = combo_find_plat.Text.ToLower();
            await FilterGames(r_title, r_platform, checkBox_mygames.Checked);
        }

        private async Task FilterGames(string r_title, string r_platform, bool mine = false)
        {
            flowLayoutBiblio.Controls.Clear();
            GameCollection gc = await new WSManager<GameCollection>("/game/allGame", WebMethode.GET).GetAsync();
            games = gc.game.ToList();
            if (!string.IsNullOrEmpty(r_title))
                games = games.Where(g => g.title.ToLower().Contains(r_title)).ToList();
            if(!string.IsNullOrEmpty(r_platform))
                games = games.Where(g => g.Platforms.Select(p => p.label.ToLower()).Contains(r_platform)).ToList();
            if (mine)
                games = games.Where(g => g.UsersID == Session.User.id).ToList();
            foreach (Game g in games)
            {
                UIBiblioGame uibg = new UIBiblioGame();
                uibg.Init(g);
                uibg.OnDetails += OnDetailBiblioGame_Click;
                flowLayoutBiblio.Controls.Add(uibg);
            }
        }

        private void btnRecentGameNext_Click(object sender, EventArgs e)
        {
            IndexGame++;
            if(IndexGame >= games.Count)
            {
                IndexGame = 0;
            }
            LoadSelectedGame();
        }

        private void btnRecentGamePrevious_Click(object sender, EventArgs e)
        {
            IndexGame--;
            if (IndexGame < 0)
            {
                IndexGame = games.Count -1;
            }
            LoadSelectedGame();
        }

        private void LoadSelectedGame()
        {
            SelectedGame = games[IndexGame];
            labelRecentGameName.Text = SelectedGame.title;
            pictureBoxRecentGame.Image = SelectedGame.Image;
        }

        private void buttonDisconnect_Click(object sender, EventArgs e)
        {
            Session.User = null;
            Close();
            LoginFormV2.Instance.Show();
        }

        private async void buttonDownload_Click(object sender, EventArgs e)
        {
            Game g = new Game() {
                id = 11,
                title = "Real Attack"
            };
            await DownloadManager.Instance.DownloadFile(g, g.title, ".exe", g.title, labelDownloadProgress, ()=>
            {
                MetaUser = DownloadManager.Instance.GetMetaUser();
                RefreshGameCollectionList();
            });
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            DownloadManager.Instance.RefreshFilesIntegrity();
            MetaUser = DownloadManager.Instance.GetMetaUser();
            RefreshGameCollectionList();
        }
    }
}
