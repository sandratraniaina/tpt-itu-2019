﻿namespace WinFormTestTPT.Views
{
    partial class FicheForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FicheForm));
            this.panelContainer = new System.Windows.Forms.Panel();
            this.buttonDownload = new System.Windows.Forms.Button();
            this.flowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.label_desc = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label_platform = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label_tags = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label_tech = new System.Windows.Forms.Label();
            this.labelGameName = new System.Windows.Forms.Label();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.panelContainer.SuspendLayout();
            this.flowLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelContainer
            // 
            this.panelContainer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(35)))), ((int)(((byte)(51)))));
            this.panelContainer.Controls.Add(this.buttonDownload);
            this.panelContainer.Controls.Add(this.flowLayoutPanel);
            this.panelContainer.Controls.Add(this.labelGameName);
            this.panelContainer.Controls.Add(this.pictureBox);
            this.panelContainer.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelContainer.Location = new System.Drawing.Point(0, 35);
            this.panelContainer.Name = "panelContainer";
            this.panelContainer.Padding = new System.Windows.Forms.Padding(5);
            this.panelContainer.Size = new System.Drawing.Size(1000, 501);
            this.panelContainer.TabIndex = 2;
            // 
            // buttonDownload
            // 
            this.buttonDownload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDownload.BackColor = System.Drawing.Color.Transparent;
            this.buttonDownload.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonDownload.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(94)))), ((int)(((byte)(161)))));
            this.buttonDownload.FlatAppearance.BorderSize = 2;
            this.buttonDownload.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDownload.Location = new System.Drawing.Point(813, 448);
            this.buttonDownload.Name = "buttonDownload";
            this.buttonDownload.Size = new System.Drawing.Size(175, 41);
            this.buttonDownload.TabIndex = 9;
            this.buttonDownload.Text = "Télécharger";
            this.buttonDownload.UseVisualStyleBackColor = false;
            this.buttonDownload.Click += new System.EventHandler(this.buttonDownload_Click);
            // 
            // flowLayoutPanel
            // 
            this.flowLayoutPanel.AutoScroll = true;
            this.flowLayoutPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(29)))), ((int)(((byte)(45)))));
            this.flowLayoutPanel.Controls.Add(this.label4);
            this.flowLayoutPanel.Controls.Add(this.label_desc);
            this.flowLayoutPanel.Controls.Add(this.label6);
            this.flowLayoutPanel.Controls.Add(this.label_platform);
            this.flowLayoutPanel.Controls.Add(this.label7);
            this.flowLayoutPanel.Controls.Add(this.label_tags);
            this.flowLayoutPanel.Controls.Add(this.label10);
            this.flowLayoutPanel.Controls.Add(this.label_tech);
            this.flowLayoutPanel.Location = new System.Drawing.Point(446, 8);
            this.flowLayoutPanel.Name = "flowLayoutPanel";
            this.flowLayoutPanel.Padding = new System.Windows.Forms.Padding(15, 0, 0, 15);
            this.flowLayoutPanel.Size = new System.Drawing.Size(542, 429);
            this.flowLayoutPanel.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Century Gothic", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.DarkOrange;
            this.label4.Location = new System.Drawing.Point(18, 10);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(507, 18);
            this.label4.TabIndex = 3;
            this.label4.Text = "DESCRIPTION";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label_desc
            // 
            this.label_desc.AutoSize = true;
            this.label_desc.Location = new System.Drawing.Point(30, 28);
            this.label_desc.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label_desc.MaximumSize = new System.Drawing.Size(450, 480);
            this.label_desc.Name = "label_desc";
            this.label_desc.Size = new System.Drawing.Size(448, 119);
            this.label_desc.TabIndex = 5;
            this.label_desc.Text = resources.GetString("label_desc.Text");
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Century Gothic", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.DarkOrange;
            this.label6.Location = new System.Drawing.Point(18, 162);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 15, 3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(507, 18);
            this.label6.TabIndex = 6;
            this.label6.Text = "PLATEFORMES";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label_platform
            // 
            this.label_platform.AutoSize = true;
            this.label_platform.Location = new System.Drawing.Point(30, 180);
            this.label_platform.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label_platform.MaximumSize = new System.Drawing.Size(440, 480);
            this.label_platform.Name = "label_platform";
            this.label_platform.Size = new System.Drawing.Size(29, 17);
            this.label_platform.TabIndex = 9;
            this.label_platform.Text = "PS4";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Century Gothic", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.DarkOrange;
            this.label7.Location = new System.Drawing.Point(18, 212);
            this.label7.Margin = new System.Windows.Forms.Padding(3, 15, 3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(490, 18);
            this.label7.TabIndex = 10;
            this.label7.Text = "TAGS";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label_tags
            // 
            this.label_tags.AutoSize = true;
            this.label_tags.Location = new System.Drawing.Point(30, 230);
            this.label_tags.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label_tags.MaximumSize = new System.Drawing.Size(440, 480);
            this.label_tags.Name = "label_tags";
            this.label_tags.Size = new System.Drawing.Size(44, 17);
            this.label_tags.TabIndex = 11;
            this.label_tags.Text = "SPORT";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Century Gothic", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.DarkOrange;
            this.label10.Location = new System.Drawing.Point(18, 262);
            this.label10.Margin = new System.Windows.Forms.Padding(3, 15, 3, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(493, 18);
            this.label10.TabIndex = 12;
            this.label10.Text = "TECHNOLOGIES";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label_tech
            // 
            this.label_tech.AutoSize = true;
            this.label_tech.Location = new System.Drawing.Point(30, 280);
            this.label_tech.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.label_tech.MaximumSize = new System.Drawing.Size(450, 480);
            this.label_tech.Name = "label_tech";
            this.label_tech.Size = new System.Drawing.Size(448, 119);
            this.label_tech.TabIndex = 13;
            this.label_tech.Text = resources.GetString("label_tech.Text");
            // 
            // labelGameName
            // 
            this.labelGameName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelGameName.BackColor = System.Drawing.Color.Transparent;
            this.labelGameName.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGameName.Location = new System.Drawing.Point(12, 449);
            this.labelGameName.Name = "labelGameName";
            this.labelGameName.Size = new System.Drawing.Size(418, 41);
            this.labelGameName.TabIndex = 4;
            this.labelGameName.Text = "Nom du jeu";
            this.labelGameName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox
            // 
            this.pictureBox.BackColor = System.Drawing.Color.Black;
            this.pictureBox.Location = new System.Drawing.Point(12, 8);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(428, 428);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(35)))), ((int)(((byte)(51)))));
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Location = new System.Drawing.Point(4, 34);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1180, 610);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Nouveautés";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(463, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(143, 19);
            this.label3.TabIndex = 2;
            this.label3.Text = "Jeux les plus suivis";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 19);
            this.label2.TabIndex = 1;
            this.label2.Text = "Jeux récents";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(35)))), ((int)(((byte)(51)))));
            this.tabPage2.Location = new System.Drawing.Point(4, 34);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1180, 610);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Mon profile";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(35)))), ((int)(((byte)(51)))));
            this.tabPage3.Location = new System.Drawing.Point(4, 34);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1180, 610);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Mes jeux";
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(35)))), ((int)(((byte)(51)))));
            this.tabPage4.Location = new System.Drawing.Point(4, 34);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1180, 610);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Autres jeux";
            // 
            // FicheForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(17)))), ((int)(((byte)(26)))));
            this.ClientSize = new System.Drawing.Size(1000, 536);
            this.Container = this.panelContainer;
            this.Controls.Add(this.panelContainer);
            this.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaximumSize = new System.Drawing.Size(1000, 536);
            this.MinimumSize = new System.Drawing.Size(1000, 536);
            this.Name = "FicheForm";
            this.Text = "Fiche du jeu";
            this.Controls.SetChildIndex(this.panelContainer, 0);
            this.panelContainer.ResumeLayout(false);
            this.flowLayoutPanel.ResumeLayout(false);
            this.flowLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panelContainer;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelGameName;
        private System.Windows.Forms.Label label_desc;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label_platform;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label_tags;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label_tech;
        private System.Windows.Forms.Button buttonDownload;
    }
}