﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinFormTestTPT.Controller.Custom;
using WinFormTestTPT.Models;
using WinFormTestTPT.Util;

namespace WinFormTestTPT.Views
{
    public partial class FicheForm : CustomForm
    {

        private Game game;

        public FicheForm()
        {
            InitializeComponent();
            //flowLayoutPanel.MouseWheel += FlowLayoutPanel_MouseWheel;
        }

        public void Init(Game game)
        {
            this.game = game;
            labelGameName.Text = game.title;
            label_desc.Text = game.description;
            label_tech.Text = Tool.Concatenate(game.Technologies.Select(p => p.label).ToArray(), "/");
            label_platform.Text = Tool.Concatenate(game.Platforms.Select(p => p.label).ToArray(), "/");
            label_tags.Text = Tool.Concatenate(game.Tags.Select(p => p.label).ToArray(), "/");
            //pictureBox.Image = game.Image;
        }

        private void FlowLayoutPanel_MouseWheel(object sender, MouseEventArgs e)
        {
            Debug.WriteLine(flowLayoutPanel.VerticalScroll.Maximum.ToString());
            //mScrollbar1.Value = flowLayoutPanel.AutoScrollPosition.Y;
            //mScrollbar1.Invalidate();
            Application.DoEvents();
        }

        private void customScrollbar1_Scroll(object sender, EventArgs e)
        {
            //flowLayoutPanel.AutoScrollPosition = new Point(0, mScrollbar1.Value);
            //mScrollbar1.Invalidate();
            Application.DoEvents();
        }

        private void buttonDownload_Click(object sender, EventArgs e)
        {
            MainForm.Instance.ProcessDameDownlad(game);
        }
    }
}
