﻿using WinFormTestTPT.Controller.Custom;

namespace WinFormTestTPT.Views
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.panelContainer = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.customTabControlc2 = new WinFormTestTPT.Util.CustomTabControlc();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.uiFavoriteGame3 = new WinFormTestTPT.Controller.Custom.UIFavoriteGame();
            this.uiFavoriteGame2 = new WinFormTestTPT.Controller.Custom.UIFavoriteGame();
            this.uiFavoriteGame1 = new WinFormTestTPT.Controller.Custom.UIFavoriteGame();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelRecentGameName = new System.Windows.Forms.Label();
            this.btnRecentGamePrevious = new System.Windows.Forms.Button();
            this.btnRecentGameNext = new System.Windows.Forms.Button();
            this.btnDetailRecentGame = new System.Windows.Forms.Button();
            this.pictureBoxRecentGame = new System.Windows.Forms.PictureBox();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.flowLayoutBiblio = new System.Windows.Forms.FlowLayoutPanel();
            this.button30 = new System.Windows.Forms.Button();
            this.customPanel2 = new WinFormTestTPT.Controller.Custom.CustomPanel();
            this.checkBox_mygames = new System.Windows.Forms.CheckBox();
            this.buttonFind = new System.Windows.Forms.Button();
            this.combo_find_plat = new WinFormTestTPT.Controller.Custom.CustomComboBox();
            this.txt_find_title = new WinFormTestTPT.Controller.Custom.CustomTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.label_user_desc = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label_fullName = new System.Windows.Forms.Label();
            this.label_mail = new System.Windows.Forms.Label();
            this.label_member_starting_from = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.buttonDisconnect = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.fl_gameCollection = new System.Windows.Forms.FlowLayoutPanel();
            this.customPanel1 = new WinFormTestTPT.Controller.Custom.CustomPanel();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.buttonDownload = new System.Windows.Forms.Button();
            this.labelDownloadProgress = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.button6 = new System.Windows.Forms.Button();
            this.labelHeaderUserName = new System.Windows.Forms.Label();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.panelContainer.SuspendLayout();
            this.panel6.SuspendLayout();
            this.customTabControlc2.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecentGame)).BeginInit();
            this.tabPage6.SuspendLayout();
            this.flowLayoutBiblio.SuspendLayout();
            this.customPanel2.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.tabPage8.SuspendLayout();
            this.panel5.SuspendLayout();
            this.customPanel1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelContainer
            // 
            this.panelContainer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(35)))), ((int)(((byte)(51)))));
            this.panelContainer.Controls.Add(this.panel6);
            this.panelContainer.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelContainer.Location = new System.Drawing.Point(0, 35);
            this.panelContainer.Name = "panelContainer";
            this.panelContainer.Size = new System.Drawing.Size(1230, 650);
            this.panelContainer.TabIndex = 2;
            // 
            // panel6
            // 
            this.panel6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.panel6.AutoScroll = true;
            this.panel6.Controls.Add(this.customTabControlc2);
            this.panel6.Location = new System.Drawing.Point(12, 3);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1210, 633);
            this.panel6.TabIndex = 1;
            // 
            // customTabControlc2
            // 
            this.customTabControlc2.Controls.Add(this.tabPage5);
            this.customTabControlc2.Controls.Add(this.tabPage6);
            this.customTabControlc2.Controls.Add(this.tabPage7);
            this.customTabControlc2.Controls.Add(this.tabPage8);
            this.customTabControlc2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.customTabControlc2.FontColor = System.Drawing.Color.White;
            this.customTabControlc2.ItemSize = new System.Drawing.Size(130, 35);
            this.customTabControlc2.Location = new System.Drawing.Point(0, 0);
            this.customTabControlc2.Name = "customTabControlc2";
            this.customTabControlc2.PanBgColor = System.Drawing.Color.Empty;
            this.customTabControlc2.SelectedIndex = 0;
            this.customTabControlc2.Size = new System.Drawing.Size(1210, 633);
            this.customTabControlc2.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.customTabControlc2.TabColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(94)))), ((int)(((byte)(161)))));
            this.customTabControlc2.TabIndex = 0;
            this.customTabControlc2.UseLightTheme = false;
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(35)))), ((int)(((byte)(51)))));
            this.tabPage5.Controls.Add(this.panel3);
            this.tabPage5.Controls.Add(this.label6);
            this.tabPage5.Controls.Add(this.label4);
            this.tabPage5.Controls.Add(this.panel2);
            this.tabPage5.Controls.Add(this.label5);
            this.tabPage5.Controls.Add(this.panel1);
            this.tabPage5.Location = new System.Drawing.Point(4, 39);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1202, 590);
            this.tabPage5.TabIndex = 0;
            this.tabPage5.Text = "Accueil";
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(17)))), ((int)(((byte)(26)))));
            this.panel3.Controls.Add(this.label7);
            this.panel3.Location = new System.Drawing.Point(471, 311);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(695, 263);
            this.panel3.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(305, 117);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(113, 17);
            this.label7.TabIndex = 0;
            this.label7.Text = "En maintenance...";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.DarkOrange;
            this.label6.Location = new System.Drawing.Point(468, 290);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(125, 18);
            this.label6.TabIndex = 7;
            this.label6.Text = "Journal de bord";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.DarkOrange;
            this.label4.Location = new System.Drawing.Point(6, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 18);
            this.label4.TabIndex = 2;
            this.label4.Text = "Jeux récents";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(17)))), ((int)(((byte)(26)))));
            this.panel2.Controls.Add(this.uiFavoriteGame3);
            this.panel2.Controls.Add(this.uiFavoriteGame2);
            this.panel2.Controls.Add(this.uiFavoriteGame1);
            this.panel2.Location = new System.Drawing.Point(471, 24);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(695, 242);
            this.panel2.TabIndex = 4;
            // 
            // uiFavoriteGame3
            // 
            this.uiFavoriteGame3.Game = null;
            this.uiFavoriteGame3.Image = null;
            this.uiFavoriteGame3.Location = new System.Drawing.Point(407, 3);
            this.uiFavoriteGame3.MaximumSize = new System.Drawing.Size(196, 235);
            this.uiFavoriteGame3.Name = "uiFavoriteGame3";
            this.uiFavoriteGame3.Size = new System.Drawing.Size(196, 235);
            this.uiFavoriteGame3.TabIndex = 2;
            // 
            // uiFavoriteGame2
            // 
            this.uiFavoriteGame2.Game = null;
            this.uiFavoriteGame2.Image = null;
            this.uiFavoriteGame2.Location = new System.Drawing.Point(205, 3);
            this.uiFavoriteGame2.MaximumSize = new System.Drawing.Size(196, 235);
            this.uiFavoriteGame2.Name = "uiFavoriteGame2";
            this.uiFavoriteGame2.Size = new System.Drawing.Size(196, 235);
            this.uiFavoriteGame2.TabIndex = 1;
            // 
            // uiFavoriteGame1
            // 
            this.uiFavoriteGame1.Game = null;
            this.uiFavoriteGame1.Image = null;
            this.uiFavoriteGame1.Location = new System.Drawing.Point(3, 3);
            this.uiFavoriteGame1.MaximumSize = new System.Drawing.Size(196, 235);
            this.uiFavoriteGame1.Name = "uiFavoriteGame1";
            this.uiFavoriteGame1.Size = new System.Drawing.Size(196, 235);
            this.uiFavoriteGame1.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.DarkOrange;
            this.label5.Location = new System.Drawing.Point(468, 3);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 18);
            this.label5.TabIndex = 6;
            this.label5.Text = "Jeux favoris";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(17)))), ((int)(((byte)(26)))));
            this.panel1.Controls.Add(this.labelRecentGameName);
            this.panel1.Controls.Add(this.btnRecentGamePrevious);
            this.panel1.Controls.Add(this.btnRecentGameNext);
            this.panel1.Controls.Add(this.btnDetailRecentGame);
            this.panel1.Controls.Add(this.pictureBoxRecentGame);
            this.panel1.Location = new System.Drawing.Point(9, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(453, 550);
            this.panel1.TabIndex = 3;
            // 
            // labelRecentGameName
            // 
            this.labelRecentGameName.BackColor = System.Drawing.Color.Transparent;
            this.labelRecentGameName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labelRecentGameName.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRecentGameName.Location = new System.Drawing.Point(3, 456);
            this.labelRecentGameName.Name = "labelRecentGameName";
            this.labelRecentGameName.Size = new System.Drawing.Size(447, 43);
            this.labelRecentGameName.TabIndex = 2;
            this.labelRecentGameName.Text = "Nom du jeu";
            this.labelRecentGameName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnRecentGamePrevious
            // 
            this.btnRecentGamePrevious.BackColor = System.Drawing.Color.Transparent;
            this.btnRecentGamePrevious.FlatAppearance.BorderSize = 0;
            this.btnRecentGamePrevious.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(17)))), ((int)(((byte)(26)))));
            this.btnRecentGamePrevious.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(86)))), ((int)(((byte)(0)))));
            this.btnRecentGamePrevious.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRecentGamePrevious.Font = new System.Drawing.Font("Century Gothic", 14.25F);
            this.btnRecentGamePrevious.Location = new System.Drawing.Point(383, 6);
            this.btnRecentGamePrevious.Name = "btnRecentGamePrevious";
            this.btnRecentGamePrevious.Size = new System.Drawing.Size(29, 30);
            this.btnRecentGamePrevious.TabIndex = 9;
            this.btnRecentGamePrevious.Text = "<";
            this.btnRecentGamePrevious.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnRecentGamePrevious.UseVisualStyleBackColor = false;
            this.btnRecentGamePrevious.Click += new System.EventHandler(this.btnRecentGamePrevious_Click);
            // 
            // btnRecentGameNext
            // 
            this.btnRecentGameNext.BackColor = System.Drawing.Color.Transparent;
            this.btnRecentGameNext.FlatAppearance.BorderSize = 0;
            this.btnRecentGameNext.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(17)))), ((int)(((byte)(26)))));
            this.btnRecentGameNext.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(86)))), ((int)(((byte)(0)))));
            this.btnRecentGameNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRecentGameNext.Font = new System.Drawing.Font("Century Gothic", 14.25F);
            this.btnRecentGameNext.Location = new System.Drawing.Point(418, 6);
            this.btnRecentGameNext.Name = "btnRecentGameNext";
            this.btnRecentGameNext.Size = new System.Drawing.Size(29, 30);
            this.btnRecentGameNext.TabIndex = 8;
            this.btnRecentGameNext.Text = ">";
            this.btnRecentGameNext.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnRecentGameNext.UseVisualStyleBackColor = false;
            this.btnRecentGameNext.Click += new System.EventHandler(this.btnRecentGameNext_Click);
            // 
            // btnDetailRecentGame
            // 
            this.btnDetailRecentGame.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(94)))), ((int)(((byte)(161)))));
            this.btnDetailRecentGame.FlatAppearance.BorderSize = 0;
            this.btnDetailRecentGame.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(17)))), ((int)(((byte)(26)))));
            this.btnDetailRecentGame.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(86)))), ((int)(((byte)(0)))));
            this.btnDetailRecentGame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDetailRecentGame.Location = new System.Drawing.Point(172, 506);
            this.btnDetailRecentGame.Name = "btnDetailRecentGame";
            this.btnDetailRecentGame.Size = new System.Drawing.Size(115, 32);
            this.btnDetailRecentGame.TabIndex = 1;
            this.btnDetailRecentGame.Text = "Détails";
            this.btnDetailRecentGame.UseVisualStyleBackColor = false;
            this.btnDetailRecentGame.Click += new System.EventHandler(this.buttonDetailRecentGame_Click);
            // 
            // pictureBoxRecentGame
            // 
            this.pictureBoxRecentGame.BackColor = System.Drawing.Color.Black;
            this.pictureBoxRecentGame.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBoxRecentGame.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxRecentGame.MaximumSize = new System.Drawing.Size(453, 453);
            this.pictureBoxRecentGame.MinimumSize = new System.Drawing.Size(453, 453);
            this.pictureBoxRecentGame.Name = "pictureBoxRecentGame";
            this.pictureBoxRecentGame.Size = new System.Drawing.Size(453, 453);
            this.pictureBoxRecentGame.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxRecentGame.TabIndex = 0;
            this.pictureBoxRecentGame.TabStop = false;
            // 
            // tabPage6
            // 
            this.tabPage6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(35)))), ((int)(((byte)(51)))));
            this.tabPage6.Controls.Add(this.flowLayoutBiblio);
            this.tabPage6.Controls.Add(this.customPanel2);
            this.tabPage6.Controls.Add(this.label8);
            this.tabPage6.Location = new System.Drawing.Point(4, 39);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(1202, 590);
            this.tabPage6.TabIndex = 1;
            this.tabPage6.Text = "Les jeux";
            // 
            // flowLayoutBiblio
            // 
            this.flowLayoutBiblio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.flowLayoutBiblio.AutoScroll = true;
            this.flowLayoutBiblio.Controls.Add(this.button30);
            this.flowLayoutBiblio.Location = new System.Drawing.Point(283, 24);
            this.flowLayoutBiblio.Name = "flowLayoutBiblio";
            this.flowLayoutBiblio.Size = new System.Drawing.Size(907, 537);
            this.flowLayoutBiblio.TabIndex = 12;
            // 
            // button30
            // 
            this.button30.Location = new System.Drawing.Point(3, 3);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(290, 192);
            this.button30.TabIndex = 26;
            this.button30.Text = "button30";
            this.button30.UseVisualStyleBackColor = true;
            // 
            // customPanel2
            // 
            this.customPanel2.BackColor = System.Drawing.Color.Transparent;
            this.customPanel2.Background = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(29)))), ((int)(((byte)(45)))));
            this.customPanel2.Controls.Add(this.checkBox_mygames);
            this.customPanel2.Controls.Add(this.buttonFind);
            this.customPanel2.Controls.Add(this.combo_find_plat);
            this.customPanel2.Controls.Add(this.txt_find_title);
            this.customPanel2.Controls.Add(this.label11);
            this.customPanel2.Controls.Add(this.label9);
            this.customPanel2.CornerRadius = 5;
            this.customPanel2.Location = new System.Drawing.Point(9, 24);
            this.customPanel2.Name = "customPanel2";
            this.customPanel2.Padding = new System.Windows.Forms.Padding(15);
            this.customPanel2.Size = new System.Drawing.Size(268, 210);
            this.customPanel2.TabIndex = 11;
            // 
            // checkBox_mygames
            // 
            this.checkBox_mygames.AutoSize = true;
            this.checkBox_mygames.Location = new System.Drawing.Point(18, 120);
            this.checkBox_mygames.Name = "checkBox_mygames";
            this.checkBox_mygames.Size = new System.Drawing.Size(78, 21);
            this.checkBox_mygames.TabIndex = 13;
            this.checkBox_mygames.Text = "Mes jeux";
            this.checkBox_mygames.UseVisualStyleBackColor = true;
            // 
            // buttonFind
            // 
            this.buttonFind.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(94)))), ((int)(((byte)(161)))));
            this.buttonFind.FlatAppearance.BorderSize = 0;
            this.buttonFind.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(17)))), ((int)(((byte)(26)))));
            this.buttonFind.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(86)))), ((int)(((byte)(0)))));
            this.buttonFind.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFind.Location = new System.Drawing.Point(18, 159);
            this.buttonFind.Name = "buttonFind";
            this.buttonFind.Size = new System.Drawing.Size(232, 32);
            this.buttonFind.TabIndex = 11;
            this.buttonFind.Text = "Valider";
            this.buttonFind.UseVisualStyleBackColor = false;
            this.buttonFind.Click += new System.EventHandler(this.buttonFind_Click);
            // 
            // combo_find_plat
            // 
            this.combo_find_plat.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.combo_find_plat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.combo_find_plat.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.combo_find_plat.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.combo_find_plat.Color3 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.combo_find_plat.Color4 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.combo_find_plat.DrawMode = System.Windows.Forms.DrawMode.Normal;
            this.combo_find_plat.DropDownHeight = 200;
            this.combo_find_plat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
            this.combo_find_plat.DropDownWidth = 232;
            this.combo_find_plat.ForeColor = System.Drawing.Color.White;
            this.combo_find_plat.IsDroppedDown = false;
            this.combo_find_plat.Location = new System.Drawing.Point(18, 87);
            this.combo_find_plat.MaxDropDownItems = 8;
            this.combo_find_plat.Name = "combo_find_plat";
            this.combo_find_plat.SelectedIndex = -1;
            this.combo_find_plat.SelectedItem = null;
            this.combo_find_plat.Size = new System.Drawing.Size(232, 23);
            this.combo_find_plat.Soreted = true;
            this.combo_find_plat.TabIndex = 10;
            // 
            // txt_find_title
            // 
            this.txt_find_title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_find_title.BackColor = System.Drawing.Color.Transparent;
            this.txt_find_title.Background = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.txt_find_title.CornerRadius = 5;
            this.txt_find_title.CustomText = "";
            this.txt_find_title.ForeColor = System.Drawing.Color.White;
            this.txt_find_title.IsPassword = false;
            this.txt_find_title.Location = new System.Drawing.Point(18, 38);
            this.txt_find_title.Margin = new System.Windows.Forms.Padding(3, 7, 3, 7);
            this.txt_find_title.MultiLine = false;
            this.txt_find_title.Name = "txt_find_title";
            this.txt_find_title.Padding = new System.Windows.Forms.Padding(15, 3, 15, 3);
            this.txt_find_title.Size = new System.Drawing.Size(232, 23);
            this.txt_find_title.TabIndex = 6;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(18, 68);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 16);
            this.label11.TabIndex = 7;
            this.label11.Text = "Plaformes";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(18, 15);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 16);
            this.label9.TabIndex = 3;
            this.label9.Text = "Nom du jeu";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.DarkOrange;
            this.label8.Location = new System.Drawing.Point(6, 3);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(90, 18);
            this.label8.TabIndex = 8;
            this.label8.Text = "Recherche";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPage7
            // 
            this.tabPage7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(35)))), ((int)(((byte)(51)))));
            this.tabPage7.Controls.Add(this.panel4);
            this.tabPage7.Location = new System.Drawing.Point(4, 39);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(1202, 590);
            this.tabPage7.TabIndex = 2;
            this.tabPage7.Text = "Mon profil";
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.Controls.Add(this.pictureBox5);
            this.panel4.Controls.Add(this.label_user_desc);
            this.panel4.Controls.Add(this.label16);
            this.panel4.Controls.Add(this.label_fullName);
            this.panel4.Controls.Add(this.label_mail);
            this.panel4.Controls.Add(this.label_member_starting_from);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.buttonDisconnect);
            this.panel4.Controls.Add(this.label15);
            this.panel4.Controls.Add(this.label13);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Location = new System.Drawing.Point(-2, 6);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1206, 558);
            this.panel4.TabIndex = 18;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(29)))), ((int)(((byte)(45)))));
            this.pictureBox5.Location = new System.Drawing.Point(18, 15);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(391, 303);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 0;
            this.pictureBox5.TabStop = false;
            // 
            // label_user_desc
            // 
            this.label_user_desc.Location = new System.Drawing.Point(428, 232);
            this.label_user_desc.Name = "label_user_desc";
            this.label_user_desc.Size = new System.Drawing.Size(764, 229);
            this.label_user_desc.TabIndex = 16;
            this.label_user_desc.Text = resources.GetString("label_user_desc.Text");
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(29)))), ((int)(((byte)(45)))));
            this.label16.Location = new System.Drawing.Point(428, 180);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(761, 34);
            this.label16.TabIndex = 0;
            this.label16.Text = "Descriptions";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_fullName
            // 
            this.label_fullName.Location = new System.Drawing.Point(694, 64);
            this.label_fullName.Name = "label_fullName";
            this.label_fullName.Size = new System.Drawing.Size(488, 23);
            this.label_fullName.TabIndex = 13;
            this.label_fullName.Text = "nom";
            this.label_fullName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label_mail
            // 
            this.label_mail.Location = new System.Drawing.Point(694, 101);
            this.label_mail.Name = "label_mail";
            this.label_mail.Size = new System.Drawing.Size(488, 23);
            this.label_mail.TabIndex = 14;
            this.label_mail.Text = "ea@gmail.com";
            this.label_mail.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label_member_starting_from
            // 
            this.label_member_starting_from.Location = new System.Drawing.Point(694, 138);
            this.label_member_starting_from.Name = "label_member_starting_from";
            this.label_member_starting_from.Size = new System.Drawing.Size(488, 23);
            this.label_member_starting_from.TabIndex = 15;
            this.label_member_starting_from.Text = "2019";
            this.label_member_starting_from.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(29)))), ((int)(((byte)(45)))));
            this.label12.Location = new System.Drawing.Point(425, 15);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(764, 34);
            this.label12.TabIndex = 0;
            this.label12.Text = "Electronic Arts";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonDisconnect
            // 
            this.buttonDisconnect.BackColor = System.Drawing.Color.DarkSlateGray;
            this.buttonDisconnect.FlatAppearance.BorderSize = 0;
            this.buttonDisconnect.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(17)))), ((int)(((byte)(26)))));
            this.buttonDisconnect.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.buttonDisconnect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDisconnect.Location = new System.Drawing.Point(18, 324);
            this.buttonDisconnect.Name = "buttonDisconnect";
            this.buttonDisconnect.Size = new System.Drawing.Size(150, 32);
            this.buttonDisconnect.TabIndex = 17;
            this.buttonDisconnect.Text = "Se déconnecter";
            this.buttonDisconnect.UseVisualStyleBackColor = false;
            this.buttonDisconnect.Click += new System.EventHandler(this.buttonDisconnect_Click);
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Century Gothic", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.DarkOrange;
            this.label15.Location = new System.Drawing.Point(428, 137);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(170, 23);
            this.label15.TabIndex = 11;
            this.label15.Text = "Membre depuis";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Century Gothic", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.DarkOrange;
            this.label13.Location = new System.Drawing.Point(428, 63);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(170, 23);
            this.label13.TabIndex = 9;
            this.label13.Text = "Nom Complet";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Century Gothic", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.DarkOrange;
            this.label14.Location = new System.Drawing.Point(428, 100);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(170, 23);
            this.label14.TabIndex = 10;
            this.label14.Text = "Mail";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabPage8
            // 
            this.tabPage8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(35)))), ((int)(((byte)(51)))));
            this.tabPage8.Controls.Add(this.panel5);
            this.tabPage8.Location = new System.Drawing.Point(4, 39);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(15);
            this.tabPage8.Size = new System.Drawing.Size(1202, 590);
            this.tabPage8.TabIndex = 3;
            this.tabPage8.Text = "Ma Bibliothèque";
            // 
            // panel5
            // 
            this.panel5.AutoScroll = true;
            this.panel5.Controls.Add(this.fl_gameCollection);
            this.panel5.Controls.Add(this.customPanel1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(15, 15);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1172, 560);
            this.panel5.TabIndex = 1;
            // 
            // fl_gameCollection
            // 
            this.fl_gameCollection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fl_gameCollection.Location = new System.Drawing.Point(0, 0);
            this.fl_gameCollection.Name = "fl_gameCollection";
            this.fl_gameCollection.Size = new System.Drawing.Size(1172, 530);
            this.fl_gameCollection.TabIndex = 0;
            // 
            // customPanel1
            // 
            this.customPanel1.BackColor = System.Drawing.Color.Transparent;
            this.customPanel1.Background = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(29)))), ((int)(((byte)(45)))));
            this.customPanel1.Controls.Add(this.buttonRefresh);
            this.customPanel1.Controls.Add(this.buttonDownload);
            this.customPanel1.Controls.Add(this.labelDownloadProgress);
            this.customPanel1.CornerRadius = 5;
            this.customPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.customPanel1.Location = new System.Drawing.Point(0, 530);
            this.customPanel1.Name = "customPanel1";
            this.customPanel1.Padding = new System.Windows.Forms.Padding(5);
            this.customPanel1.Size = new System.Drawing.Size(1172, 30);
            this.customPanel1.TabIndex = 1;
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.BackColor = System.Drawing.Color.Black;
            this.buttonRefresh.FlatAppearance.BorderSize = 0;
            this.buttonRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRefresh.Location = new System.Drawing.Point(927, 7);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(118, 23);
            this.buttonRefresh.TabIndex = 2;
            this.buttonRefresh.Text = "REFRESH";
            this.buttonRefresh.UseVisualStyleBackColor = false;
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
            // 
            // buttonDownload
            // 
            this.buttonDownload.BackColor = System.Drawing.Color.Black;
            this.buttonDownload.FlatAppearance.BorderSize = 0;
            this.buttonDownload.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDownload.Location = new System.Drawing.Point(1051, 7);
            this.buttonDownload.Name = "buttonDownload";
            this.buttonDownload.Size = new System.Drawing.Size(118, 23);
            this.buttonDownload.TabIndex = 1;
            this.buttonDownload.Text = "TEST DOWNLOAD";
            this.buttonDownload.UseVisualStyleBackColor = false;
            this.buttonDownload.Click += new System.EventHandler(this.buttonDownload_Click);
            // 
            // labelDownloadProgress
            // 
            this.labelDownloadProgress.AutoSize = true;
            this.labelDownloadProgress.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelDownloadProgress.Location = new System.Drawing.Point(5, 5);
            this.labelDownloadProgress.Name = "labelDownloadProgress";
            this.labelDownloadProgress.Size = new System.Drawing.Size(109, 17);
            this.labelDownloadProgress.TabIndex = 0;
            this.labelDownloadProgress.Text = "Téléchargement:";
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(35)))), ((int)(((byte)(51)))));
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Location = new System.Drawing.Point(4, 34);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1180, 610);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Nouveautés";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(463, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(143, 19);
            this.label3.TabIndex = 2;
            this.label3.Text = "Jeux les plus suivis";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 19);
            this.label2.TabIndex = 1;
            this.label2.Text = "Jeux récents";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(35)))), ((int)(((byte)(51)))));
            this.tabPage2.Location = new System.Drawing.Point(4, 34);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1180, 610);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Mon profile";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(35)))), ((int)(((byte)(51)))));
            this.tabPage3.Location = new System.Drawing.Point(4, 34);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1180, 610);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Mes jeux";
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(35)))), ((int)(((byte)(51)))));
            this.tabPage4.Location = new System.Drawing.Point(4, 34);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1180, 610);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Autres jeux";
            // 
            // button6
            // 
            this.button6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(94)))), ((int)(((byte)(161)))));
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(17)))), ((int)(((byte)(26)))));
            this.button6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(86)))), ((int)(((byte)(0)))));
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Location = new System.Drawing.Point(1164, 4);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(30, 27);
            this.button6.TabIndex = 7;
            this.button6.Text = "U";
            this.button6.UseVisualStyleBackColor = false;
            // 
            // labelHeaderUserName
            // 
            this.labelHeaderUserName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelHeaderUserName.BackColor = System.Drawing.Color.Transparent;
            this.labelHeaderUserName.Location = new System.Drawing.Point(997, 4);
            this.labelHeaderUserName.Name = "labelHeaderUserName";
            this.labelHeaderUserName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelHeaderUserName.Size = new System.Drawing.Size(161, 27);
            this.labelHeaderUserName.TabIndex = 1;
            this.labelHeaderUserName.Text = "user name";
            this.labelHeaderUserName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // notifyIcon
            // 
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "SharinGame";
            this.notifyIcon.Visible = true;
            this.notifyIcon.DoubleClick += new System.EventHandler(this.OnResumeNotif);
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(17)))), ((int)(((byte)(26)))));
            this.ClientSize = new System.Drawing.Size(1230, 685);
            this.Container = this.panelContainer;
            this.Controls.Add(this.labelHeaderUserName);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.panelContainer);
            this.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "MainForm";
            this.ShowSettingButton = true;
            this.Text = "SharinGame -- client v0.1";
            this.Controls.SetChildIndex(this.panelContainer, 0);
            this.Controls.SetChildIndex(this.button6, 0);
            this.Controls.SetChildIndex(this.labelHeaderUserName, 0);
            this.panelContainer.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.customTabControlc2.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRecentGame)).EndInit();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.flowLayoutBiblio.ResumeLayout(false);
            this.customPanel2.ResumeLayout(false);
            this.customPanel2.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.tabPage8.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.customPanel1.ResumeLayout(false);
            this.customPanel1.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panelContainer;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private Util.CustomTabControlc customTabControlc2;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBoxRecentGame;
        private System.Windows.Forms.Button btnDetailRecentGame;
        private System.Windows.Forms.Button btnRecentGameNext;
        private System.Windows.Forms.Label labelRecentGameName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnRecentGamePrevious;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Button buttonDisconnect;
        private System.Windows.Forms.Label label_user_desc;
        private System.Windows.Forms.Label label_member_starting_from;
        private System.Windows.Forms.Label label_mail;
        private System.Windows.Forms.Label label_fullName;
        private Controller.Custom.CustomTextBox txt_find_title;
        private Controller.Custom.CustomComboBox combo_find_plat;
        private Controller.Custom.CustomPanel customPanel2;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label labelHeaderUserName;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutBiblio;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button buttonFind;
        private System.Windows.Forms.CheckBox checkBox_mygames;
        private UIFavoriteGame uiFavoriteGame3;
        private UIFavoriteGame uiFavoriteGame2;
        private UIFavoriteGame uiFavoriteGame1;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.FlowLayoutPanel fl_gameCollection;
        private CustomPanel customPanel1;
        private System.Windows.Forms.Label labelDownloadProgress;
        private System.Windows.Forms.Button buttonDownload;
        private System.Windows.Forms.Button buttonRefresh;
    }
}