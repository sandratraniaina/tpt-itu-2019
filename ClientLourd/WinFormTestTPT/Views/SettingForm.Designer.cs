﻿namespace WinFormTestTPT.Views
{
    partial class SettingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelContainer = new System.Windows.Forms.Panel();
            this.customTabControlc1 = new WinFormTestTPT.Util.CustomTabControlc();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.buttonDisconnect = new System.Windows.Forms.Button();
            this.checkBoxAutoConnect = new System.Windows.Forms.CheckBox();
            this.panelContainer.SuspendLayout();
            this.customTabControlc1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelContainer
            // 
            this.panelContainer.Controls.Add(this.customTabControlc1);
            this.panelContainer.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelContainer.Location = new System.Drawing.Point(0, 35);
            this.panelContainer.Name = "panelContainer";
            this.panelContainer.Size = new System.Drawing.Size(475, 74);
            this.panelContainer.TabIndex = 6;
            // 
            // customTabControlc1
            // 
            this.customTabControlc1.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.customTabControlc1.Controls.Add(this.tabPage1);
            this.customTabControlc1.Controls.Add(this.tabPage2);
            this.customTabControlc1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.customTabControlc1.FontColor = System.Drawing.Color.White;
            this.customTabControlc1.ItemSize = new System.Drawing.Size(25, 150);
            this.customTabControlc1.Location = new System.Drawing.Point(0, 0);
            this.customTabControlc1.Multiline = true;
            this.customTabControlc1.Name = "customTabControlc1";
            this.customTabControlc1.PanBgColor = System.Drawing.Color.Empty;
            this.customTabControlc1.SelectedIndex = 0;
            this.customTabControlc1.Size = new System.Drawing.Size(475, 74);
            this.customTabControlc1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.customTabControlc1.TabColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(94)))), ((int)(((byte)(161)))));
            this.customTabControlc1.TabIndex = 0;
            this.customTabControlc1.UseLightTheme = false;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(35)))), ((int)(((byte)(51)))));
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(154, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(317, 66);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Application";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "En maintenance...";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(35)))), ((int)(((byte)(51)))));
            this.tabPage2.Controls.Add(this.buttonDisconnect);
            this.tabPage2.Controls.Add(this.checkBoxAutoConnect);
            this.tabPage2.Location = new System.Drawing.Point(154, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(317, 66);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Compte";
            // 
            // buttonDisconnect
            // 
            this.buttonDisconnect.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDisconnect.BackColor = System.Drawing.Color.LightSlateGray;
            this.buttonDisconnect.FlatAppearance.BorderSize = 0;
            this.buttonDisconnect.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(24)))), ((int)(((byte)(36)))));
            this.buttonDisconnect.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(24)))), ((int)(((byte)(36)))));
            this.buttonDisconnect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDisconnect.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDisconnect.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.buttonDisconnect.Location = new System.Drawing.Point(10, 35);
            this.buttonDisconnect.Margin = new System.Windows.Forms.Padding(0);
            this.buttonDisconnect.Name = "buttonDisconnect";
            this.buttonDisconnect.Size = new System.Drawing.Size(302, 28);
            this.buttonDisconnect.TabIndex = 4;
            this.buttonDisconnect.Text = "Se déconnecter";
            this.buttonDisconnect.UseVisualStyleBackColor = false;
            // 
            // checkBoxAutoConnect
            // 
            this.checkBoxAutoConnect.AutoSize = true;
            this.checkBoxAutoConnect.Location = new System.Drawing.Point(10, 11);
            this.checkBoxAutoConnect.Name = "checkBoxAutoConnect";
            this.checkBoxAutoConnect.Size = new System.Drawing.Size(171, 21);
            this.checkBoxAutoConnect.TabIndex = 0;
            this.checkBoxAutoConnect.Text = "Connexion automatique";
            this.checkBoxAutoConnect.UseVisualStyleBackColor = true;
            // 
            // SettingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(475, 109);
            this.Container = this.panelContainer;
            this.Controls.Add(this.panelContainer);
            this.Name = "SettingForm";
            this.Text = "Paramètres";
            this.Controls.SetChildIndex(this.panelContainer, 0);
            this.panelContainer.ResumeLayout(false);
            this.customTabControlc1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelContainer;
        private Util.CustomTabControlc customTabControlc1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.CheckBox checkBoxAutoConnect;
        private System.Windows.Forms.Button buttonDisconnect;
        private System.Windows.Forms.Label label1;
    }
}