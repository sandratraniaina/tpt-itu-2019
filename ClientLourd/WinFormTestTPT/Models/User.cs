﻿using Newtonsoft.Json;
using System;

namespace WinFormTestTPT.Models
{
    public class User : BaseModel
    {
        public string name { get; set; }
        public string lastname { get; set; }
        public string username { get; set; }
        public string email { get; set; }
        public string description { get; set; }
        public DateTime datesignup { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
