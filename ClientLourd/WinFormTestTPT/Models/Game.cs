﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormTestTPT.Models
{
    public class Game : BaseModel
    {
        private const string IMG_PATH_FOLDER = @"E:\TPT\";

        public string title { get; set; }
        public string description { get; set; }
        public DateTime releaseDate { get; set; }
        public bool validadmin { get; set; }
        public string link { get; set; }
        public long UsersID { get; set; }
        public Platform[] Platforms { get; set; }
        public Technologie[] Technologies { get; set; }
        public Tag[] Tags { get; set; }
        public Img[] images { get; set; }
        
        public Image Image {
            get {
                string fullPath = IMG_PATH_FOLDER + images[0].picture;
                Console.WriteLine(" ---------------  " + fullPath);
                return Image.FromFile(@fullPath);
            }
        }
        //*/
        public static implicit operator MetaGame(Game game)
        {
            return new MetaGame() { id = game.id };
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
