﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormTestTPT.Models
{
    public class Platform : BaseModel
    {
        public string label { get; set; }
    }

    public class Technologie : BaseModel
    {
        public string label { get; set; }
    }

    public class Tag : BaseModel
    {
        public string label { get; set; }
    }

    public class Img : BaseModel
    {
        public string picture { get; set; }
    }
}
