﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormTestTPT.Models
{
    public class BaseModel
    {
        public long id { get; set; }
        public static implicit operator bool(BaseModel u) => u != null;
    }
}
