﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormTestTPT.Util
{
    public abstract class Constants
    {
        public static readonly Color COLOR_DARK = Color.FromArgb(0, 17, 26);
        public static readonly Color COLOR_DARK_LIGHT = Color.FromArgb(0, 35, 51);
        public static readonly Color COLOR_BLUE = Color.FromArgb(0, 94, 161);
        public static readonly Color COLOR_ORANGE = Color.FromArgb(229, 86, 0);
        public static readonly int WM_NCLBUTTONDOWN = 0xA1;
        public static readonly int HT_CAPTION = 0x2;
        public static readonly string GAME_DIRECTORY_NAME = "applications";
    }
}
