﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using WinFormTestTPT.Models;

namespace WinFormTestTPT.Util
{
    public class WSManager<T> where T : BaseModel
    {
        private static readonly string URL_BASE = "https://server-tptm2.herokuapp.com/api";
        private WebMethode M = WebMethode.GET;
        private string endpoint;
        private NameValueCollection keyValuePairs;

        private string json;

        public delegate void OnWebResponse(string resultString);
        public delegate void OnSingular(T result);
        public delegate void OnMultiple(List<T> results);

        public WSManager(string endpoint, WebMethode M, NameValueCollection keyValuePairs = null)
        {
            this.M = M;
            this.endpoint = endpoint;
            this.keyValuePairs = keyValuePairs;
            Console.WriteLine("URL: " + URL_BASE + endpoint);
            Init();
        }

        private void Init()
        {
            switch (M)
            {
                case WebMethode.GET:
                    using (var client = new WebClient())
                    {
                        client.Encoding = Encoding.UTF8;
                        json = client.DownloadString(URL_BASE + endpoint);
                    }
                    break;
                case WebMethode.POST:
                    using (var client = new WebClient())
                    {
                        try
                        {
                            var response = client.UploadValues(URL_BASE + endpoint, keyValuePairs);
                            json = Encoding.UTF8.GetString(response);
                        }
                        catch (System.Net.WebException)
                        {
                            json = string.Empty;
                        }
                        
                    }
                    break;
                case WebMethode.PUT:
                    break;
                case WebMethode.DELETE:
                    break;
            }
        }

        public void GetString(OnWebResponse onResponse)
        {
            onResponse(json);
        }

        public void Get(OnSingular on)
        {
            on(JsonConvert.DeserializeObject<T>(json));
        }

        public async Task<T> GetAsync()
        {
            T res = null;
            await Task.Run(() => {
                res = JsonConvert.DeserializeObject<T>(json);
            });
            return res;
        }

        public void GetAll(OnMultiple on)
        {
            on(JsonHelper.FromJson<T>(json).ToList());
        }

        public async Task<List<T>> GetAllAsync()
        {
            List<T> res = new List<T>();
            await Task.Run(() =>
            {
                res = JsonHelper.FromJson<T>(json).ToList();
            });
            return res;
        }
    }
}
