﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinFormTestTPT.Models;

namespace WinFormTestTPT.Util
{
    public static class Session
    {
        public static User User { get; set; } = null;
        public static List<Game> Games { get; set; } = new List<Game>();
    }
}
