﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormTestTPT.Util
{
    public class Tool
    {
        public static void PaintGradientBackground(Control sender, PaintEventArgs e, Color a, Color b, float angle)
        {
            Graphics graphics = e.Graphics;
            RectangleF gradient_rectangle = new RectangleF(0, 0, sender.Width, sender.Height);
            Brush br = new System.Drawing.Drawing2D.LinearGradientBrush(gradient_rectangle, a, b, angle);
            graphics.FillRectangle(br, gradient_rectangle);
        }

        public static void RoundForm(Form form, int radius)
        {
            form.FormBorderStyle = FormBorderStyle.None;
            form.Region = Region.FromHrgn(NativeMethods.RoundedForm(0,0,form.Width, form.Height, radius, radius));
        }

        public static GraphicsPath RoundedRect(Rectangle bounds, int radius)
        {
            int diameter = radius * 2;
            Size size = new Size(diameter, diameter);
            Rectangle arc = new Rectangle(bounds.Location, size);
            GraphicsPath path = new GraphicsPath();
            if (radius == 0)
            {
                path.AddRectangle(bounds);
                return path;
            }
            path.AddArc(arc, 180, 90);
            arc.X = bounds.Right - diameter;
            path.AddArc(arc, 270, 90);
            arc.Y = bounds.Bottom - diameter;
            path.AddArc(arc, 0, 90);
            arc.X = bounds.Left;
            path.AddArc(arc, 90, 90);
            path.CloseFigure();
            return path;
        }

        public static void DrawRoundedRectangle(Graphics graphics, Pen pen, Rectangle bounds, int cornerRadius)
        {
            if (graphics == null)
                throw new ArgumentNullException("graphics");
            if (pen == null)
                throw new ArgumentNullException("pen");

            using (GraphicsPath path = RoundedRect(bounds, cornerRadius))
            {
                graphics.DrawPath(pen, path);
            }
        }

        public static void FillRoundedRectangle(Graphics graphics, Brush brush, Rectangle bounds, int cornerRadius)
        {
            if (graphics == null)
                throw new ArgumentNullException("graphics");
            if (brush == null)
                throw new ArgumentNullException("brush");

            using (GraphicsPath path = RoundedRect(bounds, cornerRadius))
            {
                graphics.FillPath(brush, path);
            }
        }

        public static string Concatenate(string[] values, string separator)
        {
            string res = "";
            if (values.Length == 1)
            {
                res = values[0];
            }
            else if(values.Length > 1)
            {
                res = values[0];
                for (int i = 1; i < values.Length; i++)
                    res += separator + values[i];
            }
            return res;
        }

        public static string FixBase64ForImage(string Image)
        {
            StringBuilder sbText = new StringBuilder(Image, Image.Length);
            sbText = sbText.Replace("\r\n", String.Empty);
            sbText = sbText.Replace(" ", String.Empty);
            return sbText.ToString();
        }
    }
}
