﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinFormTestTPT.Models;

namespace WinFormTestTPT.Util
{
    public class DownloadManager
    {
        private static DownloadManager _instance = null;
        private static readonly string URI = @"ftp://hobs@ftp.drivehq.com/TptM2/";

        public static bool IsDownloading { get; private set; }

        private string gameFolder = AppDomain.CurrentDomain.BaseDirectory + Constants.GAME_DIRECTORY_NAME;
        private NetworkCredential credential = new NetworkCredential("hobs", "tpt2019M2!");
        private Stopwatch sw = new Stopwatch();

        private MetaUser metaUser;

        public delegate void OnDownloadComplete();

        public static DownloadManager Instance {
            get {
                if (_instance == null)
                    _instance = new DownloadManager();
                return _instance;
            }
        }

        public MetaUser GetMetaUser()
        {
            return metaUser;
        }

        public async Task DownloadFile(Game game, string fileToDownload, string fileExtension, string saveToFIleName, Label progressLabel, OnDownloadComplete onComplete)
        {
            if (IsDownloading)
            {
                MessageBox.Show("Un fichier est en cours de téléchargement, veuillez atendre d'il soit terminé.");
                return;
            }
            try
            {
                IsDownloading = true;
                string url = URI + fileToDownload + fileExtension;
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(url);
                request.Credentials = credential;
                string path = gameFolder + "\\" + saveToFIleName + fileExtension;
                string metaPath = gameFolder + "\\" + saveToFIleName + ".meta";
                Console.WriteLine(path);
                Console.WriteLine(metaPath);

                progressLabel.Text = "Vérification des données...";
                var responseFileDownload = await request.GetResponseAsync();
                responseFileDownload = (FtpWebResponse)responseFileDownload;
                var responseStream = responseFileDownload.GetResponseStream();
                var writeStream = new FileStream(@path, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite, bufferSize: 4096, useAsync: true);

                progressLabel.Text = "Vérification du fichier à télécharger...";
                WebRequest sizeRequest = WebRequest.Create(url);
                sizeRequest.Credentials = credential;
                sizeRequest.Method = WebRequestMethods.Ftp.GetFileSize;
                var x = await sizeRequest.GetResponseAsync();
                double size = x.ContentLength;

                progressLabel.Text = "Démarrage du télécharger...";
                int Length = 4096;
                Byte[] buffer = new Byte[Length];
                int bytesRead = await responseStream.ReadAsync(buffer, 0, Length);
                double total = bytesRead;
                sw.Start();
                while (bytesRead > 0)
                {
                    await writeStream.WriteAsync(buffer, 0, bytesRead);
                    //bytesRead = responseStream.Read(buffer, 0, Length);
                    bytesRead = await responseStream.ReadAsync(buffer, 0, Length);
                    total += bytesRead;
                    double _tot = (total / 1000000d);
                    progressLabel.Text = string.Format("Téléchargement en cours: {0} - {1}/{2}Mo ~ {3} Kb/s", 
                        game.title, 
                        _tot.ToString("F3", CultureInfo.CreateSpecificCulture("sv-SE")), 
                        (size / 1000000).ToString("F3", CultureInfo.CreateSpecificCulture("sv-SE")),
                        (bytesRead / 4096d / sw.Elapsed.TotalSeconds * 10000).ToString("0.00"));
                }
                responseStream.Close();
                writeStream.Close();
                progressLabel.Text = "Téléchargement terminé.";
                IsDownloading = false;
                sw.Stop();

                //write meta files
                MetaGame meta = game;
                string metaJson = JsonConvert.SerializeObject(meta);
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(metaPath, false))
                {
                    file.WriteLine(metaJson);
                }

                RefreshFilesIntegrity();
                onComplete.Invoke();
            }
            catch (Exception ex)
            {
                progressLabel.Text = "Erreur de téléchargement.";
                IsDownloading = false;
                sw.Stop();
                Console.WriteLine(ex.Message);
            }
        }

        public bool GameExistInFolder(Game game)
        {
            foreach (string file in Directory.EnumerateFiles(gameFolder, "*.meta"))
            {
                string contents = File.ReadAllText(file);
                if (game.id == JsonConvert.DeserializeObject<MetaGame>(contents).id)
                    return true;
            }
            return false;
        }

        public void RefreshFilesIntegrity()
        {
            string userMetaPath = gameFolder + "\\" + "data.json";

            //load all saved games
            List<long> metaGames = new List<long>();
            foreach (string file in Directory.EnumerateFiles(gameFolder, "*.meta"))
            {
                string contents = File.ReadAllText(file);
                metaGames.Add(JsonConvert.DeserializeObject<MetaGame>(contents).id);
            }

            //override user met file
            MetaUser u_meta = new MetaUser()
            {
                UserID = Session.User.id,
                gameCollection = metaGames
            };
            metaUser = u_meta;
            string userMetaJson = JsonConvert.SerializeObject(u_meta);
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(userMetaPath, false))
            {
                file.WriteLine(userMetaJson);
            }
        }
    }
}
