﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ComponentModel.Design;
using WinFormTestTPT.Util;
using System.Drawing.Drawing2D;

namespace WinFormTestTPT.Controller.Custom
{
    public partial class CustomTextBox : UserControl
    {
        #region CustomProperties
        private bool ML;
        private Color BC;
        private int CR = 5;
        private bool isPassword = false;

        [Category("Custom")] public bool MultiLine { get { return textBox.Multiline;} set { ML = value; textBox.Multiline = value; } }
        [Category("Custom")] public bool IsPassword { get => isPassword; set { isPassword = textBox.UseSystemPasswordChar = value; } }
        [Category("Custom")] public int CornerRadius { get { return CR; } set { CR = value; Refresh(); } }
        [Category("Custom")] public Color Background { get => BC; set { base.BackColor = Color.Transparent; if(value != Color.Transparent)textBox.BackColor = value; BC = value; Refresh(); } }
        [Category("Custom")] public override Color ForeColor { get => base.ForeColor; set { base.ForeColor = value; textBox.ForeColor = value; } }
        [Category("Custom")] public string CustomText { get => Text; set { Text = value; } }
        #endregion

        [Category("Custom")] public override string Text { get { return textBox.Text; } set { textBox.Text = value; Refresh(); } }

        public CustomTextBox()
        {
            ML = false;
            BC = Color.White;
            Padding = new Padding(10, 5, 10, 5);
            InitializeComponent();
            textBox.BackColor = Background;
            BackColor = Color.Transparent;
            Background = Color.White;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Bitmap B = new Bitmap(Width, Height);
            Graphics G = Graphics.FromImage(B);
            Rectangle x2 = new Rectangle(0, 0, Width, Height);
            /*ColorBlend myblend = new ColorBlend();
            myblend.Colors = new[] { BC, BC, BC }; // colors
            myblend.Positions = new[] { 0.0F, 0.5F, 1.0F };
            LinearGradientBrush lgBrush = new LinearGradientBrush(x2, Color.Black, Color.Black, 90.0F);
            lgBrush.InterpolationColors = myblend;
            //*/
            G.SmoothingMode = SmoothingMode.HighQuality;

            SolidBrush b = new SolidBrush(BC);

            Tool.FillRoundedRectangle(G, b, x2, CR);
            e.Graphics.DrawImage(B.Clone() as Image, 0, 0);
            G.Dispose(); B.Dispose();
        }
    }
}
