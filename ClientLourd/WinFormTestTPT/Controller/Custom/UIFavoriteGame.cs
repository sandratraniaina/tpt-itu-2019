﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinFormTestTPT.Models;

namespace WinFormTestTPT.Controller.Custom
{
    public partial class UIFavoriteGame : UserControl
    {
        private Image _image;
        private Game _game;

        [Category("Custom")] public Image Image { get => _image; set { _image = value; pictureBox.Image = _image; Refresh(); } }
        [Browsable(false)] public Game Game {
            get => _game;
            set {
                _game = value;
                //if (_game)Image = _game.Image;
            }
        }

        [Category("Custom")] public event EventHandler OnDetails;

        public static implicit operator Game(UIFavoriteGame fav)
        {
            return fav.Game;
        }

        public UIFavoriteGame()
        {
            InitializeComponent();
        }

        private void buttonDetails_Click(object sender, EventArgs e)
        {
            OnDetails?.Invoke(this, e);
        }
    }
}
