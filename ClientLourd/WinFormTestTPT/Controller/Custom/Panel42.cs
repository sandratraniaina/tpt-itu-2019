﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormTestTPT.Controller.Custom
{
    [Designer("System.Windows.Forms.Design.ParentControlDesigner, System.Design", typeof(IDesigner))]
    public partial class Panel42 : UserControl
    {
        private Color BC;
        private int BW = 1;
        private Image IMG;

        public Color BorderColor { get { return BC; } set { BC = value; Refresh(); } }
        public int BorderWidth { get { return BW; } set { BW = value; Refresh(); } }
        public Image LeftImage { get { return IMG; } set { IMG = value; this.Image22.Image = IMG; Refresh(); } }

        public Panel42()
        {
            InitializeComponent();
        }

        public Panel42(IContainer container)
        {
            container.Add(this);
            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Bitmap B = new Bitmap(Width, Height);
            Graphics G = Graphics.FromImage(B);
            Pen p = new Pen(BorderColor, BW);

            //draw border
            G.DrawRectangle(p, new Rectangle(0, 0, Width-1, Height-1));

            //change ImageLeft source
            
            //end process
            e.Graphics.DrawImage(B.Clone() as Image, 0, 0);
            G.Dispose(); B.Dispose();
        }
    }
}
