﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinFormTestTPT.Models;

namespace WinFormTestTPT.Controller.Custom
{
    public partial class UIBiblioGame : UserControl
    {
        private Game _game;
        
        [Category("Custom")] public Game Game { get => _game; set => _game = value; }
        [Category("Custom")] public event EventHandler OnDetails;

        public static implicit operator Game(UIBiblioGame _ref)
        {
            return _ref.Game;
        }

        public UIBiblioGame()
        {
            InitializeComponent();
        }

        private void buttonDetails_Click(object sender, EventArgs e)
        {
            OnDetails?.Invoke(this, e);
        }

        public void Init(Game game)
        {
            _game = game;
            label_title.Text = _game.title;
        }
    }
}
