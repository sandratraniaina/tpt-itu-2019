﻿namespace WinFormTestTPT.Controller.Custom
{
    partial class Panel42
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Image22 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.Image22)).BeginInit();
            this.SuspendLayout();
            // 
            // Image22
            // 
            this.Image22.Dock = System.Windows.Forms.DockStyle.Left;
            this.Image22.Location = new System.Drawing.Point(0, 0);
            this.Image22.Name = "Image22";
            this.Image22.Size = new System.Drawing.Size(200, 200);
            this.Image22.TabIndex = 0;
            this.Image22.TabStop = false;
            // 
            // Panel42
            // 
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.Image22);
            this.Name = "Panel42";
            this.Size = new System.Drawing.Size(400, 200);
            ((System.ComponentModel.ISupportInitialize)(this.Image22)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.PictureBox Image22;
    }
}
