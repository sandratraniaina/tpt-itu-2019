﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using WinFormTestTPT.Models;
using WinFormTestTPT.Util;
using System.Drawing.Drawing2D;
using WinFormTestTPT.Views;

namespace WinFormTestTPT.Controller.Custom
{
    public partial class CustomForm : Form
    {

        private Color bg = Color.FromArgb(255, 0, 35, 51);
        private Color borderColor = Color.DarkOrange;
        private int topBarSize = 35;
        private Panel container;
        private bool showSettingButton;

        private const int HTCLIENT = 0x1;
        private const int HTCAPTION = 0x2;
        private const int WM_NCHITTEST = 0x84;
        private const int WM_NCPAINT = 0x0085;
        private bool m_aeroEnabled;
        private const int CS_DROPSHADOW = 0x00020000;
        
        [Category("Custom")] public Color Background { get => bg; set { bg = value; Refresh(); } }
        [Category("Custom")] public Color BorderColor { get => borderColor; set { borderColor = value; Refresh(); } }
        [Category("Custom")] public override string Text { get => base.Text; set { base.Text = value; labelHeader.Text = value; } }
        [Category("Custom")] public new Panel Container { get => container; set { container = value; if (value != null) { container.Height = Height - topBarSize; Refresh(); } } }
        [Category("Custom")] public bool ShowSettingButton { get => showSettingButton; set { showSettingButton = value; buttonSetting.Visible = showSettingButton; Refresh(); } }

        public CustomForm()
        {
            InitializeComponent();
            buttonSetting.Visible = showSettingButton;
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
        }

        protected override CreateParams CreateParams
        {
            get
            {
                m_aeroEnabled = CheckAeroEnabled();
                CreateParams cp = base.CreateParams;
                if (!m_aeroEnabled)
                    cp.ClassStyle |= CS_DROPSHADOW;
                //cp.ExStyle |= 0x02000000;
                //cp.Style &= ~0x02000000;
                return cp;
            }
        }
        protected override void OnPaintBackground(PaintEventArgs e)
        {
            Bitmap B = new Bitmap(Width, topBarSize);
            Graphics G = Graphics.FromImage(B);
            Rectangle x2 = new Rectangle(0, 0, Width, topBarSize);
            ColorBlend myblend = new ColorBlend();
            myblend.Colors = new[] { Color.FromArgb(255, 0, 17, 26), Color.FromArgb(255, 0, 17, 26) }; // colors gradient
            myblend.Positions = new[] { 0.0F, 1.0F };
            LinearGradientBrush lgBrush = new LinearGradientBrush(x2, Color.Black, Color.Black, 90.0F);
            lgBrush.InterpolationColors = myblend;
            G.SmoothingMode = SmoothingMode.AntiAlias;
            G.FillRectangle(lgBrush, x2);
            //Tool.FillRoundedRectangle(G, lgBrush, x2, cornerRadius);

            Rectangle rect = new Rectangle(0, 0, Width - 1, topBarSize);
            Brush linearGradientBrush = new LinearGradientBrush(rect, borderColor, Color.FromArgb(255, 0, 35, 51), 90);

            e.Graphics.DrawImage(B.Clone() as Image, 0, 0);
            e.Graphics.DrawRectangle(new Pen(linearGradientBrush, 1), rect);
            G.Dispose(); B.Dispose();
        }
        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case WM_NCPAINT:
                    if (m_aeroEnabled)
                    {
                        var v = 2;
                        NativeMethods.DwmSetWindowAttribute(this.Handle, 2, ref v, 4);
                        MARGINS margins = new MARGINS()
                        {
                            bottomHeight = 1,
                            leftWidth = 0,
                            rightWidth = 0,
                            topHeight = 0
                        }; NativeMethods.DwmExtendFrameIntoClientArea(this.Handle, ref margins);
                    }
                    break;
                default: break;
            }
            base.WndProc(ref m);
            if (m.Msg == WM_NCHITTEST && (int)m.Result == HTCLIENT) m.Result = (IntPtr)HTCAPTION;
        }
        private bool CheckAeroEnabled()
        {
            if (Environment.OSVersion.Version.Major >= 6)
            {
                int enabled = 0;
                NativeMethods.DwmIsCompositionEnabled(ref enabled);
                return (enabled == 1) ? true : false;
            }
            return false;
        }

        public virtual void OnExit() { }

        public virtual void ExitLabel_Click(object sender, EventArgs e)
        {
            OnExit();
            Dispose();
        }
        public virtual void label_minimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }
        private void OnFormMouseDown(object sender, MouseEventArgs e)
        {
            /*
            if (e.Button == MouseButtons.Left)
            {
                Tool.ReleaseCapture();
                Tool.SendMessage(Handle, Constants.WM_NCLBUTTONDOWN, Constants.HT_CAPTION, 0);
            }//*/
        }
        private void OnSizeChanged(object sender, EventArgs e)
        {
            if(container != null)
            {
                container.Height = Height - topBarSize;
            }
            buttonSetting.Location = new Point(Width - 30, buttonSetting.Location.Y);
            Refresh();
        }

        private void buttonSetting_Click(object sender, EventArgs e)
        {
            SettingForm setting = new SettingForm();
            setting.ShowDialog(this);
        }
    }
}
