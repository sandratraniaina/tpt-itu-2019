﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using WinFormTestTPT.Util;

namespace WinFormTestTPT.Util
{
    public class CustomTabControlc : TabControl
    {
        private Color MainColor;
        private Color PanelBgColor;
        private Color TextColor;
        private bool LightTheme;

        public bool UseLightTheme
        {
            get
            {
                return LightTheme;
            }
            set
            {
                LightTheme = value;
                Refresh();
            }
        }

        public Color PanBgColor
        {
            get
            {
                return PanelBgColor;
            }
            set
            {
                PanelBgColor = value;
                Refresh();
            }
        }

        public Color TabColor
        {
            get
            {
                return MainColor;
            }
            set
            {
                MainColor = value;
                Refresh();
            }
        }

        public Color FontColor
        {
            get
            {
                return TextColor;
            }
            set
            {
                TextColor = value;
                Refresh();
            }
        }

        public CustomTabControlc()
        {
            SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.ResizeRedraw | ControlStyles.UserPaint | ControlStyles.OptimizedDoubleBuffer, true);
            DoubleBuffered = true;
            SizeMode = TabSizeMode.Fixed;
            ItemSize = new Size(100, 40);
            Size = new Size(400, 250);
            MainColor = Color.DeepPink;
            TextColor = Color.White;
            LightTheme = false;
            Alignment = TabAlignment.Top;
        }

        public Pen ToPen(Color color)
        {
            return new Pen(color);
        }

        public Brush ToBrush(Color color)
        {
            return new SolidBrush(color);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Bitmap B = new Bitmap(Width, Height);
            Graphics G = Graphics.FromImage(B);

            // tabpage color
            if (LightTheme == false)
            {
                try
                {
                    SelectedTab.BackColor = Color.FromArgb(255, 0, 35, 51);
                }
                catch
                {
                }
                G.Clear(Color.FromArgb(255, 0, 35, 51));
            }
            else
            {
                try
                {
                    SelectedTab.BackColor = Color.FromArgb(255, 220, 220, 220);
                }
                catch
                {
                }
                G.Clear(Color.FromArgb(255, 220, 220, 220));
            }

            // tab bar
            if (LightTheme == false)
            {
                switch (Alignment)
                {
                    case TabAlignment.Top: G.FillRectangle(new SolidBrush(Color.Transparent), new Rectangle(0, 0, Width, ItemSize.Height)); break;
                    case TabAlignment.Bottom: G.FillRectangle(new SolidBrush(Color.Transparent), new Rectangle(0, Height - ItemSize.Height, Width, ItemSize.Height)); break;
                    case TabAlignment.Left: G.FillRectangle(new SolidBrush(Color.Transparent), new Rectangle(0, 0, ItemSize.Height, Height)); break;
                    case TabAlignment.Right: G.FillRectangle(new SolidBrush(Color.Transparent), new Rectangle(Width - ItemSize.Height, 0, ItemSize.Height, Height)); break;
                }
            }
            else
            {
                switch (Alignment)
                {
                    case TabAlignment.Top: G.FillRectangle(new SolidBrush(Color.FromArgb(255, 200, 200, 200)), new Rectangle(0, 0, Width, ItemSize.Height)); break;
                    case TabAlignment.Bottom: G.FillRectangle(new SolidBrush(Color.FromArgb(255, 200, 200, 200)), new Rectangle(0, Height - ItemSize.Height, Width, ItemSize.Height)); break;
                    case TabAlignment.Left: G.FillRectangle(new SolidBrush(Color.FromArgb(255, 200, 200, 200)), new Rectangle(0, 0, ItemSize.Height, Height)); break;
                    case TabAlignment.Right: G.FillRectangle(new SolidBrush(Color.FromArgb(255, 200, 200, 200)), new Rectangle(Width - ItemSize.Height, 0, ItemSize.Height, Height)); break;
                }
            }

            // selected tab stuff
            for (var i = 0; i <= TabCount - 1; i++)
            {
                Rectangle x2 = new Rectangle(GetTabRect(i).Location.X + Padding.X, GetTabRect(i).Location.Y + Padding.Y, GetTabRect(i).Width - Padding.X * 2, GetTabRect(i).Height - Padding.Y * 2);
                if (i == SelectedIndex)
                {
                    ColorBlend myblend = new ColorBlend();
                    myblend.Colors = new [] {
                        this.MainColor,
                        this.MainColor
                    }; // colors
                    myblend.Positions = new[] { 0.0F, 1.0F };
                    LinearGradientBrush lgBrush = new LinearGradientBrush(x2, Color.Black, Color.Black, 90.0F);
                    lgBrush.InterpolationColors = myblend;
                    G.SmoothingMode = SmoothingMode.HighQuality;
                    G.FillRectangle(lgBrush, x2);
                    //Tool.FillRoundedRectangle(G, lgBrush, x2, 5);
                    // AREA 1
                    // paste here.........................................................................................
                    if (ImageList != null)
                    {
                        try
                        {
                            if (ImageList.Images[TabPages[i].ImageIndex] != null)
                            {
                                G.DrawImage(ImageList.Images[TabPages[i].ImageIndex], new Point(x2.Location.X + 8, x2.Location.Y + 6));
                                G.DrawString("      " + TabPages[i].Text, Font, this.ToBrush(this.TextColor), x2, new StringFormat() { LineAlignment = StringAlignment.Center, Alignment = StringAlignment.Center });
                            }
                            else
                                G.DrawString(TabPages[i].Text, Font, this.ToBrush(this.TextColor), x2, new StringFormat() { LineAlignment = StringAlignment.Center, Alignment = StringAlignment.Center });
                        }
                        catch (Exception)
                        {
                            G.DrawString(TabPages[i].Text, Font, this.ToBrush(this.TextColor), x2, new StringFormat() { LineAlignment = StringAlignment.Center, Alignment = StringAlignment.Center });
                        }
                    }
                    else
                        G.DrawString(TabPages[i].Text, Font, this.ToBrush(this.TextColor), x2, new StringFormat() { LineAlignment = StringAlignment.Center, Alignment = StringAlignment.Center });
                    // ...................................................................................................

                    if (LightTheme == false)
                    {
                        G.DrawLine(new Pen(Color.FromArgb(255, 0, 35, 51)), new Point(x2.Location.X - 1, x2.Location.Y - 1), new Point(x2.Location.X, x2.Location.Y));
                        G.DrawLine(new Pen(Color.FromArgb(255, 0, 35, 51)), new Point(x2.Location.X - 1, x2.Bottom - 1), new Point(x2.Location.X, x2.Bottom));
                    }
                    else
                    {
                        G.DrawLine(new Pen(Color.FromArgb(255, 255, 255, 255)), new Point(x2.Location.X - 1, x2.Location.Y - 1), new Point(x2.Location.X, x2.Location.Y));
                        G.DrawLine(new Pen(Color.FromArgb(255, 255, 255, 255)), new Point(x2.Location.X - 1, x2.Bottom - 1), new Point(x2.Location.X, x2.Bottom));
                    }
                }
                else
                {
                    // unselected tab color
                    if (LightTheme == false)
                        G.FillRectangle(new SolidBrush(Color.Transparent), x2);
                    else
                        G.FillRectangle(new SolidBrush(Color.FromArgb(255, 255, 255, 255)), x2);
                    
                    // paste here......................................................................................
                    if (LightTheme == false)
                    {
                        if (ImageList != null)
                        {
                            try
                            {
                                if (ImageList.Images[TabPages[i].ImageIndex] != null)
                                {
                                    G.DrawImage(ImageList.Images[TabPages[i].ImageIndex], new Point(x2.Location.X + 8, x2.Location.Y + 6));
                                    G.DrawString("      " + TabPages[i].Text, Font, Brushes.White, x2, new StringFormat() { LineAlignment = StringAlignment.Center, Alignment = StringAlignment.Center });
                                }
                                else
                                    G.DrawString(TabPages[i].Text, Font, Brushes.White, x2, new StringFormat() { LineAlignment = StringAlignment.Center, Alignment = StringAlignment.Center });
                            }
                            catch (Exception)
                            {
                                G.DrawString(TabPages[i].Text, Font, Brushes.White, x2, new StringFormat() { LineAlignment = StringAlignment.Center, Alignment = StringAlignment.Center });
                            }
                        }
                        else
                            G.DrawString(TabPages[i].Text, Font, Brushes.White, x2, new StringFormat() { LineAlignment = StringAlignment.Center, Alignment = StringAlignment.Center });
                    }
                    else if (LightTheme == true)
                    {
                        if (ImageList != null)
                        {
                            try
                            {
                                if (ImageList.Images[TabPages[i].ImageIndex] != null)
                                {
                                    G.DrawImage(ImageList.Images[TabPages[i].ImageIndex], new Point(x2.Location.X + 8, x2.Location.Y + 6));
                                    G.DrawString("      " + TabPages[i].Text, Font, ToBrush(Color.FromArgb(255, 64, 64, 64)), x2, new StringFormat() { LineAlignment = StringAlignment.Center, Alignment = StringAlignment.Center });
                                }
                                else
                                    G.DrawString(TabPages[i].Text, Font, ToBrush(Color.FromArgb(255, 64, 64, 64)), x2, new StringFormat() { LineAlignment = StringAlignment.Center, Alignment = StringAlignment.Center });
                            }
                            catch (Exception)
                            {
                                G.DrawString(TabPages[i].Text, Font, ToBrush(Color.FromArgb(255, 64, 64, 64)), x2, new StringFormat() { LineAlignment = StringAlignment.Center, Alignment = StringAlignment.Center });
                            }
                        }
                        else
                            G.DrawString(TabPages[i].Text, Font, ToBrush(Color.FromArgb(255, 64, 64, 64)), x2, new StringFormat() { LineAlignment = StringAlignment.Center, Alignment = StringAlignment.Center });
                    }
                }
            }
            // tab bar \ panel separator
            if (LightTheme == false)
            {
                switch (Alignment)
                {
                    case TabAlignment.Top: G.DrawLine(ToPen(this.MainColor), new Point(0, ItemSize.Height -1), new Point(Width, ItemSize.Height -1)); break;
                    case TabAlignment.Bottom: G.DrawLine(ToPen(this.MainColor), new Point(0, Height - ItemSize.Height), new Point(Width, Height - ItemSize.Height)); break;
                    case TabAlignment.Left: G.DrawLine(ToPen(this.MainColor), new Point(ItemSize.Height - 4, 0), new Point(ItemSize.Height - 4, Height)); break;
                    case TabAlignment.Right: G.DrawLine(ToPen(this.MainColor), new Point(Width - ItemSize.Height, 0), new Point(Width - ItemSize.Height, Height)); break;
                }
            }
            else
            {
                switch (Alignment)
                {
                    case TabAlignment.Top: G.DrawLine(new Pen(Color.FromArgb(255, 234, 234, 234)), new Point(0, ItemSize.Height), new Point(Width, ItemSize.Height)); break;
                    case TabAlignment.Bottom: G.DrawLine(new Pen(Color.FromArgb(255, 234, 234, 234)), new Point(0, Height - ItemSize.Height), new Point(Width, Height - ItemSize.Height)); break;
                    case TabAlignment.Left: G.DrawLine(new Pen(Color.FromArgb(255, 234, 234, 234)), new Point(ItemSize.Height, 0), new Point(ItemSize.Height, Height)); break;
                    case TabAlignment.Right: G.DrawLine(new Pen(Color.FromArgb(255, 234, 234, 234)), new Point(Width - ItemSize.Height, 0), new Point(Width - ItemSize.Height, Height)); break;
                }
            }
            e.Graphics.DrawImage(B.Clone() as Image, 0, 0);
            G.Dispose(); B.Dispose();
        }
    }
}