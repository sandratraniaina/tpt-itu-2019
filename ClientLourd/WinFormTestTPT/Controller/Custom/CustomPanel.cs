﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ComponentModel.Design;
using System.Drawing.Drawing2D;
using WinFormTestTPT.Util;

namespace WinFormTestTPT.Controller.Custom
{
    [Designer("System.Windows.Forms.Design.ParentControlDesigner, System.Design", typeof(IDesigner))]
    public partial class CustomPanel : Panel
    {
        private Color background = Color.FromArgb(255, 0, 29, 45);
        private int cornerRadius = 5;

        public Color Background { get => background; set { BackColor = Color.Transparent; background = value; Refresh(); } }
        public int CornerRadius { get { return cornerRadius; } set { cornerRadius = value; Refresh(); } }

        public CustomPanel()
        {
            InitializeComponent();
            Padding = new Padding(25);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Bitmap B = new Bitmap(Width, Height);
            Graphics G = Graphics.FromImage(B);
            Rectangle x2 = new Rectangle(0, 0, Width, Height);
            ColorBlend myblend = new ColorBlend();
            myblend.Colors = new[] { background, background }; // colors
            myblend.Positions = new[] { 0.0F, 1.0F };
            LinearGradientBrush lgBrush = new LinearGradientBrush(x2, Color.Black, Color.Black, 90.0F);
            lgBrush.InterpolationColors = myblend;
            G.SmoothingMode = SmoothingMode.HighQuality;
            Tool.FillRoundedRectangle(G, lgBrush, x2, cornerRadius);
            e.Graphics.DrawImage(B.Clone() as Image, 0, 0);
            G.Dispose(); B.Dispose();
        }
    }
}
